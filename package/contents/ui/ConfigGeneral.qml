import QtQuick 2.15
import QtQuick.Controls
import QtQuick.Layouts 1.1
import org.kde.kcmutils as KCM

KCM.SimpleKCM {
    property alias cfg_updateInterval: updateInterval.value
    property alias cfg_makeFontBold: makeFontBold.checked
    property alias cfg_showChargingStatus: showChargingStatus.checked

    ColumnLayout {
        RowLayout {
            Label {
                id: updateIntervalLabel
                text: i18n(" Update interval:")
            }
            Slider {
                id: updateInterval
                from: 0.1
                to: 5
            }
            Label {
                id: updateIntervalValueLabel
                text: Math.round(updateInterval.value * 10) / 10 + " s"
            }
        }

        CheckBox {
            id: makeFontBold
            text: i18n("Bold Text")
        }

        CheckBox {
            id: showChargingStatus
            text: i18n("Show Charging Status")
        }
    }
}
